import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ThreePaletteComponent} from "./colors/components/three-palette/three-palette.component";
import {EditThreePaletteComponent} from "./colors/components/edit-three-palette/edit-three-palette.component";
import {AddThreePaletteComponent} from "./colors/components/add-three-palette/add-three-palette.component";

const routes: Routes = [
  {
    path: '',
    component: ThreePaletteComponent,
  },
  {
    path: 'edit',
    component: EditThreePaletteComponent,
  },
  {
    path: 'add',
    component: AddThreePaletteComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
