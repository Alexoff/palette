import {Action} from '@ngrx/store';
import {Color} from "../models/color.model";

export enum ColorActionTypes {
  LoadColors = '[Color] Load colors action',
  LoadColorsSuccess = '[Color] Load colors success action',

  LoadColor = '[Color] Load color action',
  LoadColorSuccess = '[Color] Load color success action',

  SelectColor = '[Color] Select color action',

  SetColor = '[Color] Set color action',

  UpdateColor = '[Color] Update color action',
  UpdateColorSuccess = '[Color] Update color success action',

  AddColor = '[Color] Add color action',
  AddColorSuccess = '[Color] Add color success action'
}

export class LoadColorsAction implements Action {
  public readonly type = ColorActionTypes.LoadColors;
}

export class LoadColorsSuccessAction implements Action {
  public readonly type = ColorActionTypes.LoadColorsSuccess;

  constructor(public colors: Color[]) {
  }
}

export class LoadColorAction implements Action {
  public readonly type = ColorActionTypes.LoadColor;

  constructor(public id: number) {
  }
}

export class LoadColorSuccessAction implements Action {
  public readonly type = ColorActionTypes.LoadColorSuccess;

  constructor(public color: Color) {
  }
}

export class SelectColorAction implements Action {
  public readonly type = ColorActionTypes.SelectColor;

  constructor(public num: number) {
  }
}

export class SetColorAction implements Action {
  public readonly type = ColorActionTypes.SetColor;
}

export class UpdateColorAction implements Action {
  public readonly type = ColorActionTypes.UpdateColor;

  constructor(public id: number, public color: Color) {
  }
}

export class UpdateColorSuccessAction implements Action {
  public readonly type = ColorActionTypes.UpdateColorSuccess;

  constructor(public color: Color) {
  }
}

export class AddColorAction implements Action {
  public readonly type = ColorActionTypes.AddColor;

  constructor(public color: Color) {
  }
}

export class AddColorSuccessAction implements Action {
  public readonly type = ColorActionTypes.AddColorSuccess;

  constructor(public color: Color) {
  }
}

export type ColorAction =
  LoadColorsAction |
  LoadColorsSuccessAction |
  LoadColorAction |
  LoadColorSuccessAction |
  SelectColorAction |
  SetColorAction |
  UpdateColorAction |
  UpdateColorSuccessAction |
  AddColorAction |
  AddColorSuccessAction
