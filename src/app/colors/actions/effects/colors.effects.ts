import {Injectable} from "@angular/core";
import {Actions, Effect, ofType} from "@ngrx/effects";
import {Store} from "@ngrx/store";
import {ColorService} from "../../services/color.service";

import {
  AddColorAction,
  AddColorSuccessAction,
  ColorActionTypes,
  LoadColorAction,
  LoadColorsAction,
  LoadColorsSuccessAction,
  LoadColorSuccessAction,
  UpdateColorAction,
  UpdateColorSuccessAction
} from '../colors.actions'
import {State} from "../reducer/colors.reducers";
import {map, switchMap} from "rxjs/operators";
import {Color} from "../../models/color.model";

@Injectable()
export class ColorsEffects {

  constructor(private actions: Actions,
              private colorService: ColorService,
              public store: Store<State>) {
  }

  @Effect()
  loadColors = this.actions.pipe(
    ofType(ColorActionTypes.LoadColors),
    switchMap((action: LoadColorsAction) => {
      return this.colorService.getColors().pipe(
        map((colors: Color[]) => new LoadColorsSuccessAction(colors))
      )
    })
  );

  @Effect()
  loadColor = this.actions.pipe(
    ofType(ColorActionTypes.LoadColor),
    switchMap((action: LoadColorAction) => {
      return this.colorService.getColor(action.id).pipe(
        map((color: Color) => new LoadColorSuccessAction(color))
      )
    })
  );

  @Effect()
  updateColor = this.actions.pipe(
    ofType(ColorActionTypes.UpdateColor),
    switchMap((action: UpdateColorAction) => {
      return this.colorService.updateColor(action.id, action.color).pipe(
        map((color: Color) => new UpdateColorSuccessAction(color))
      )
    })
  );

  @Effect()
  addColor = this.actions.pipe(
    ofType(ColorActionTypes.AddColor),
    switchMap((action: AddColorAction) => {
      return this.colorService.addColor(action.color).pipe(
        map((color: Color) => new AddColorSuccessAction(color))
      )
    })
  )
}
