import {ColorAction, ColorActionTypes} from "../colors.actions";
import {Color} from "../../models/color.model";

export interface State {
  colors: Color[];
  color: any;
  selected_color: number;
}

const initialState: State = {
  colors: [],
  color: null,
  selected_color: 1,
};

export function colorReducer(state = initialState, action: ColorAction) {
  switch (action.type) {
    case ColorActionTypes.LoadColorsSuccess:
      return {
        ...state,
        colors: action.colors,
      };
    case ColorActionTypes.LoadColorSuccess:
      return {
        ...state,
        color: action.color,
      };
    case ColorActionTypes.SelectColor:
      return {
        ...state,
        selected_color: action.num,
      };
    case ColorActionTypes.SetColor:
      return {
        ...state,
        color: setColor(),
      };
    case ColorActionTypes.UpdateColorSuccess:
      updateColors(state.colors, action.color);
      return {
        ...state,
        color: action.color,
      };
    case ColorActionTypes.AddColorSuccess:
      addColors(state.colors, action.color);
      return {
        ...state,
        color: action.color,
      };
    default:
      return state;
  }
}

export function setColor() {
  let json = {
    "name_colors": "",
    "one": "#ffffff",
    "two": "#ffffff",
    "three": "#ffffff"
  };
  return new Color(json);
}

export function updateColors(colors, color) {
  colors.forEach((col, i) => {
    if (col.id === color.id) {
      colors[i] = color;
    }
  });
  return colors;
}

export function addColors(colors, color) {
  colors.push(color);
  return colors;
}
