import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ThreePaletteComponent} from "./components/three-palette/three-palette.component";
import {StoreModule} from "@ngrx/store";
import {colorReducer} from "./actions/reducer/colors.reducers";
import {ColorService} from "./services/color.service";
import {EffectsModule} from "@ngrx/effects";
import {ColorsEffects} from "./actions/effects/colors.effects";
import {HeaderComponent} from "./header/header.component";
import {EditThreePaletteComponent} from "./components/edit-three-palette/edit-three-palette.component";
import {ColorPickerModule} from "ngx-color-picker";
import {FormsModule} from "@angular/forms";
import {AddThreePaletteComponent} from "./components/add-three-palette/add-three-palette.component";

@NgModule({
  declarations: [
    ThreePaletteComponent,
    AddThreePaletteComponent,
    EditThreePaletteComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    StoreModule.forRoot({color: colorReducer}),
    EffectsModule.forRoot([ColorsEffects]),
    ColorPickerModule,
    FormsModule
  ],
  exports: [
    HeaderComponent
  ],
  providers: [ColorService]
})
export class ColorsModule { }
