import {Component, OnInit} from '@angular/core';
import {Color} from "../../models/color.model";
import {Store} from "@ngrx/store";
import {State} from "../../actions/reducer/colors.reducers";
import {AddColorAction, SetColorAction} from "../../actions/colors.actions";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-three-palette',
  templateUrl: './add-three-palette.component.html',
  styleUrls: ['./add-three-palette.component.scss']
})
export class AddThreePaletteComponent implements OnInit {
 public color;
 public selectedColor: number;

 constructor(private  store: Store<State>, private router: Router) {
 }

  ngOnInit(): void {
   this.store.dispatch(new SetColorAction());
    this.store.select('color').subscribe(state => {
      this.selectedColor = state.selected_color;
      this.color = state.color;
    })
  }

  public addColor() {
   this.store.dispatch(new AddColorAction(this.color));
   this.router.navigate([''])
  }

  set selectedColors(color) {
    switch (this.selectedColor) {
      case 1:
        this.color.one = color;
        break;
      case 2:
        this.color.two = color;
        break;
      case 3:
        this.color.three = color;
        break
    }
  }

  get selectedColors() {
    switch (this.selectedColor) {
      case 1:
        return this.color.one;
      case 2:
        return this.color.two;
      case 3:
        return this.color.three
    }
  }
}
