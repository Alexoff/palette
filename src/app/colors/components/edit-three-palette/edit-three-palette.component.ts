import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {State} from "../../actions/reducer/colors.reducers";
import {LoadColorAction, UpdateColorAction} from "../../actions/colors.actions";
import {Color} from "../../models/color.model";

@Component({
  selector: 'app-edit-three-palette',
  templateUrl: './edit-three-palette.component.html',
  styleUrls: ['./edit-three-palette.component.scss']
})
export class EditThreePaletteComponent implements OnInit {
  public color: Color;
  public selectedColor: number;

  constructor(private  store: Store<State>, private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(param => {
      if (param && param.id) {
        this.store.dispatch(new LoadColorAction(param.id))
      }
    });
  }

  ngOnInit(): void {
    this.store.select('color').subscribe(state => {
      this.color = state.color;
      this.selectedColor = state.selected_color;
    })
  }

  updateColor() {
    this.store.dispatch(new UpdateColorAction(this.color.id, this.color));
    this.router.navigate(['']);
  }

  set selectedColors(color) {
    switch (this.selectedColor) {
      case 1:
        this.color.one = color;
        break;
      case 2:
        this.color.two = color;
        break;
      case 3:
        this.color.three = color;
        break
    }
  }

  get selectedColors() {
    switch (this.selectedColor) {
      case 1:
        return this.color.one;
      case 2:
        return this.color.two;
      case 3:
        return this.color.three
    }
  }
}
