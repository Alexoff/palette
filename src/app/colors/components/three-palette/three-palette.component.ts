import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {Color} from "../../models/color.model";
import {LoadColorAction, LoadColorsAction} from "../../actions/colors.actions";
import {State} from "../../actions/reducer/colors.reducers";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-three',
  templateUrl: './three-palette.component.html',
  styleUrls: ['./three-palette.component.scss']
})
export class ThreePaletteComponent implements OnInit{
  public colors: Color[];

  constructor(private  store: Store<State>, private router: Router) {
    this.store.select('color').subscribe((state) => {
      this.colors = state.colors;
    })
  }

  ngOnInit(): void {

  }

  routeToEditColor(id: number) {
    this.router.navigate(['/edit'], {queryParams: {id: id}})
  }

  routeToAddColor() {
    this.router.navigate(['/add'])
  }

}
