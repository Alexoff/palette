import { Component } from '@angular/core';
import {Store} from "@ngrx/store";
import {State} from "../actions/reducer/colors.reducers";
import {LoadColorsAction, SelectColorAction} from "../actions/colors.actions";
import {Color} from "../models/color.model";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  public color: Color;
  public isEdit = false;

  constructor(private  store: Store<State>, private router: Router, private route: ActivatedRoute) {
    this.store.dispatch(new LoadColorsAction());
    this.store.select('color').subscribe((state) => {
      this.color = state.color;
    });
    this.route.queryParams.subscribe(param => {
      if (param && param.id) this.isEdit = true;
      else this.isEdit = false;
    });
  }


  selectColor(num: number) {
    this.store.dispatch(new SelectColorAction(num));
  }
}
