export class Color {
  id: number;
  name_colors: string;
  one: number;
  two: number;
  three: number;

  constructor(json: any) {
    this.parse(json);
  }

  private parse(json) {
    if (json.id) {
      this.id = json.id;
    }
    if (json.name_colors) {
      this.name_colors = json.name_colors;
    }
    if (json.one) {
      this.one = json.one;
    }
    if (json.two) {
      this.two = json.two;
    }
    if (json.three) {
      this.three = json.three;
    }
  }
}
