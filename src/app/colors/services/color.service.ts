import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Color} from "../models/color.model";
import {map} from "rxjs/operators";

@Injectable()
export class ColorService {
  host: string = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  public getColors() {
    return this.http.get(`${this.host}/colors`)
      .pipe(
        map((colors: Color[]) => colors.map(color => new Color(color)))
      )
  }

  public getColor(id: number) {
    return this.http.get(`${this.host}/colors/${id}`)
      .pipe(
        map((color: any) => new Color(color))
      )
  }

  public updateColor(id: number, color: Color) {
    return this.http.put(`${this.host}/colors/${id}`, color)
      .pipe(
        map((color: any) => new Color(color))
      )
  }

  public addColor(color: Color) {
    return this.http.post(`${this.host}/colors`, color)
      .pipe(
        map((color: any) => new Color(color))
      )
  }
}
